#ifndef SERVER_HPP
#define SERVER_HPP

#include <string>

class Client {
public:
    Client(int fd);
    ~Client();
private:
    int fd;
};

class Server {
public:
    Server();
    ~Server();
    void run();
private:
    static std::string getVar(std::string key);
    static std::string getVar_demo(std::string key);
    void sockFile();
    int sockFile_fd;
    int net_fd;
};


    // unlink("/tmp/mysocket");
#endif // !SERVER_HPP
