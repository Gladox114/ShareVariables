

A simple bridge between a socket file and many PCs connected to it over the network.

Sending the name of the variable to the socket file will return the value of the variable that the connected PC may or may not hold.

If more than one PC is connected then the hostname needs to be included like "hostname:variable".
If no hostname is included then all PCs are asked for the variable and the first one who answers with a value will be returned. If all PCs answered with a NULL then a NULL will be returned.
