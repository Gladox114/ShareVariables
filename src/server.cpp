// internal includes
#include "server.hpp"

// libraries needed
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstdlib>
#include <poll.h>
#include <unistd.h>
#include <string>
#include <map>
#include <sys/un.h>
#include <thread>

// Server Functions
// ----------------


Client::Client(int fd) {
    this->fd = fd;
}

Client::~Client() {

}


Server::Server() {
    this->run();
}

Server::~Server() {

}

void Server::run() {
    std::thread myThread(&Server::sockFile, this);
    for (;;) {
        printf("hi\n");
        sleep(2);
    }
}

std::string Server::getVar(std::string key) {
    // for a single connection, the first PC can be taken out that is connected
    // for multiple connections, the hostname needs to be passed like "hostname:var" to get its value
}

std::string Server::getVar_demo(std::string key) {
    std::string val;
    std::map<std::string, std::string> outsideVars {
        {"cpu", "44%"},
        {"gpu", "22%"},
        {"test", "hi"}
    };

    // search for the val
    if (outsideVars.find(key) != outsideVars.end()) {
        // return the val
        val = outsideVars[key];
    } else {
        val = "NULL";
    }
    return val;
}

void Server::sockFile() {
    // create socket for socket file
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        perror("socket");
        exit(1);
    }

    unlink("/tmp/mysocket");

    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, "/tmp/mysocket", sizeof(addr.sun_path)-1);

    if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(fd, 5) == -1) {
        perror("listen");
        exit(1);
    }

    // rest of your code to handle incoming connections
    for (;;) {
        int new_fd = accept(fd, NULL, NULL);
        Client *client = new Client(new_fd);

        char buf[256];
        int length = read(new_fd, buf, sizeof buf);
        buf[length] = '\0'; // just to be sure, end it with a null character
        if (length == -1) { 
            perror("read");
            exit(EXIT_FAILURE);
        }
        printf("status: %i\n", length);
        printf("Income: %s\n",buf);
        printf("hex: ");
        for (size_t i = 0; i < length; i++) {
            printf("%x ", buf[i]);
        }
        printf("\n");
        std::string key = buf;
        // if there is a new carriage return character then remove it 
        if (key.back() == '\n') {
            key.pop_back();
        }

        // std::string val = this->getVar_demo(key);
        std::string val = this->getVar_demo(key);

        // send it
        send(new_fd, val.c_str(), val.size() , 0);
        close(new_fd);
    }
}

